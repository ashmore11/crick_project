setup:
	git config core.fileMode false
	npm install

watch:
	node_modules/.bin/polvo -ws

release:
	node_modules/.bin/polvo -r
	
pull:
	git pull origin master

push:
	git push origin master