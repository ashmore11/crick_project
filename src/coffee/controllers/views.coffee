class View

	UNIQUE_ID  	= 0


	###
	Hash Map to store the views:

	hash_model = {
		"<view_name>" : [ <view_instance>, <view_instance>, .. ],
		"<view_name>" : [ <view_instance>, <view_instance>, .. ]
	}
	###
	hash_model  : {}


	###
	Uid Map. Internal map used for easily get a view by uid

	uid_map = {
		"<UNIQUE_ID>" : { name : <view_name>, index: <view_index> },
		"<UNIQUE_ID>" : { name : <view_name>, index: <view_index> },
		  ...
	}
	###
	uid_map: {}




	by_dom: ( dom ) -> @by_uid $( dom ).data 'uid'

	by_uid: ( uid ) =>
		if not @uid_map[ uid ]? then return false

		name = @uid_map[ uid ].name
		index = @uid_map[ uid ].index

		return @hash_model[ name ][ index ]

	# Get the view from the hash model
	get: ( id, index = 0 ) =>
		unless @hash_model[ id ]?
			# console.error "View #{id} #{index} doesn't exists"
			return false

		@hash_model[ id ][ index ]

	bind: ( scope = 'body', tolog = false ) ->

		# console.error "Bindings views: #{scope}"
		$( scope ).find( '[data-view]' ).each( ( index, item ) =>

			$item = $ item

			view_name = $item.data( 'view' )

			$item.removeAttr 'data-view'

			if view_name.substring(0, 1) is "["
				names = view_name.substring(1, view_name.length - 1).split(",")
			else
				names = [view_name]

			for name in names
				@_add_view $item, name

			

			# remove the data-view attribute, so it won't be instantiated twice!
			$item.removeAttr 'data-view'

			

		).promise().done => $( @ ).trigger "after_bind"



	_add_view: ( $item, view_name ) ->

		try
			view = require "app/views/#{view_name}"
		catch e
			console.warn 'e ->', e.message
			console.error "app/views/#{view} not found for ", $item

		view = new view $item

		# Save the view in a hash model
		@hash_model[ view_name ] ?= []

		l = @hash_model[ view_name ].length

		@hash_model[ view_name ][ l ] = view


		# Save the incremental uid to the dom and to the instance
		view.uid = UNIQUE_ID
		view.view_name = view_name

		$item.attr 'data-uid', UNIQUE_ID

		# Save the view in a linear array model
		@uid_map[ UNIQUE_ID ] =
			name  : view_name
			index : @hash_model[ view_name ].length - 1


		UNIQUE_ID++




	on_view_destroyed: ( uid ) ->
		
		if not @uid_map[ uid ]? then return false

		# Get the data from the uid map
		name  = @uid_map[ uid ].name
		index = @uid_map[ uid ].index

		# delete the reference in the model
		if @hash_model[ name ][ index ]?

			# delete the item from the uid_map
			delete @uid_map[ uid ]

			# Delete the item from the hash_model
			@hash_model[ name ].splice index, 1

			# Update the index on the uid_map for the views left of the same type
			for item, i in @hash_model[ name ]
				@uid_map[ item.uid ].index = i


				



view = new View

module.exports = view


# exporting get method for window, so you can retrieve views just with View( id )
window.View = view