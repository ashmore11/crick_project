delay = ( ms, funk ) -> setTimeout funk, ms

module.exports = class Management

	constructor: ( @dom ) ->

		jQuery(window).on 'resize', @resize
		@resize()

	
	resize: =>

		if window.innerWidth < 640
			document.getElementsByClassName('section_one')[0].style.height = 'auto'
		else
			document.getElementsByClassName('section_one')[0].style.height = document.getElementsByClassName('bkg')[0].clientHeight + 'px'

		width = document.getElementsByClassName('left_column')[0].clientWidth - 84

		for i in [ 0..document.querySelectorAll('.entry_four').length - 1 ]
			document.getElementsByClassName('entry_four')[i].style.width = width + 'px'