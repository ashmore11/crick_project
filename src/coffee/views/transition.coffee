delay    = ( ms, funk ) -> setTimeout funk, ms
interval = ( ms, funk ) -> setInterval funk, ms

module.exports = class Transition

	current_index: 0
	current_index_2: 0

	constructor: ( @dom ) ->

		@slideshow    = @dom.find '.pdf_slideshow'
		@pdf_ul       = @dom.find '.pdf_slideshow ul.slides'
		@pdf_li       = @dom.find '.pdf_slideshow li.slide'
		@slide_ul     = @dom.find '.right_column ul.slides'
		@slide_li     = @dom.find '.right_column li.slide'
		@prev_slide   = @dom.find '.pdf_slideshow .prev'
		@next_slide   = @dom.find '.pdf_slideshow .next'
		@left_column  = @dom.find '.left_column'
		@link         = @dom.find '.section_two li.entry a'
		@shapes_left  = @dom.find '.bkg .shapes.left'
		@shapes_right = @dom.find '.bkg .shapes.right'

		do @handle_slideshows

		jQuery(window).on 'resize', @resize
		@resize()


	handle_slideshows: =>

		@next_slide.click =>

			if @current_index is @pdf_li.length - 1 then return

			@current_index += 1

			@pdf_li.removeClass 'current'
			jQuery( @pdf_li[ @current_index ] ).addClass 'current'


		@prev_slide.click =>

			if @current_index is 0 then return

			@current_index -= 1

			@pdf_li.removeClass 'current'
			jQuery( @pdf_li[ @current_index ] ).addClass 'current'


		interval 4000, =>
			
			if @current_index_2 is @slide_li.length - 1 then @current_index_2 = 0 else @current_index_2 += 1

			@slide_li.removeClass 'current'
			jQuery( @slide_li[ @current_index_2 ] ).addClass 'current'


	resize: =>

		width = @left_column.width() - 85
		@link.width width

		@slideshow.height @pdf_li.height() + 120
		@pdf_ul.height @pdf_li.height() + 20

		@slide_ul.height @slide_li.height()

		left  = @slideshow.offset().left - @shapes_left.width()
		right = jQuery( window ).width() - @slideshow.offset().left
		
		@shapes_left.css left: left
		@shapes_right.css left: right
