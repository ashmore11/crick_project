delay = ( ms, funk ) -> setTimeout funk, ms

module.exports = class Pagination

	current_index: 0

	constructor: ( @dom ) ->

		@ul  = @dom.find('ul.pagination')
		@li  = @dom.find('li.page')
		@job = @dom.find('.job')

		jQuery( @li[ i ] ).attr 'data-index', i for i in [ 0..@li.length ]

		@next_page = @dom.find('.next_page')
		@prev_page = @dom.find('.prev_page')
		@button    = @dom.find('.page_nav_button')
		@page_sel  = @dom.find('.page_nav_button.page')
		@selector  = @dom.find('.page_nav_button.selector')

		@first_page = @dom.find('.page_nav_button.first_page')
		@last_page  = @dom.find('.page_nav_button.last_page')

		@last_page.find('a').html @li.length

		@selector_0 = jQuery( @selector[ 0 ] )
		@selector_1 = jQuery( @selector[ 1 ] )

		@selector_0.attr 'data-id', Math.floor( ( @li.length / 2 ) / 2 ) 
		@selector_1.attr 'data-id', Math.floor( ( @li.length / 2 ) + @li.length / 4 ) 

		@page_sel_0 = jQuery(@page_sel[0])
		@page_sel_1 = jQuery(@page_sel[1])
		@page_sel_2 = jQuery(@page_sel[2])

		@page_sel_0.find('a').html Math.floor( @li.length / 2 ) - 1
		@page_sel_1.find('a').html Math.floor( @li.length / 2 ) 
		@page_sel_2.find('a').html Math.floor( @li.length / 2 ) + 1
		
		@page_sel_0.attr 'data-id', Math.floor( @li.length / 2 ) - 2
		@page_sel_1.attr 'data-id', Math.floor( @li.length / 2 ) - 1
		@page_sel_2.attr 'data-id', Math.floor( @li.length / 2 )

		@dom.find('.page_nav a').on 'click', -> event.preventDefault()

		jQuery( window ).on 'resize', => do @resize
		@resize()

		jQuery( @li[ 0 ] ).addClass 'current'

		@first_page.addClass 'active'

		@first_page.on 'click', => do @first_page_selected
		@last_page.on 'click',  => do @last_page_selected
		@selector.on 'click',   => do @selector_clicked
		@page_sel.on 'click',   => do @page_selected
		@next_page.on 'click',  => do @next_clicket
		@prev_page.on 'click',  => do @prev_clicked


	first_page_selected: =>

		@li.removeClass 'current'
		jQuery( @li[ 0 ] ).addClass 'current'

		@current_index = 0

		@button.removeClass 'active'
		@first_page.addClass 'active'


	last_page_selected: =>

		@li.removeClass 'current'
		jQuery( @li[ @li.length - 1 ] ).addClass 'current'

		@current_index = @li.length - 1

		@button.removeClass 'active'
		@last_page.addClass 'active'


	selector_clicked: =>

		index = jQuery(event.currentTarget).data('id')

		@li.removeClass 'current'
		jQuery( @li[ index ] ).addClass 'current'

		@button.removeClass 'active'


	page_selected: =>

		index = jQuery(event.currentTarget).data('id')
		
		@li.removeClass 'current'
		jQuery( @li[ index ] ).addClass 'current'

		@button.removeClass 'active'
		jQuery( event.currentTarget ).addClass 'active'

		@current_index = index

	
	next_clicket: =>

		if @current_index is @li.length - 1 then return

		@current_index += 1

		@li.removeClass 'current'
		jQuery( @li[ @current_index ] ).addClass 'current'

		@button.removeClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is @li.length - 1 then @last_page.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 ) - 2 then @page_sel_0.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 ) - 1 then @page_sel_1.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 )     then @page_sel_2.addClass 'active'


	prev_clicked: =>

		if @current_index is 0 then return

		@current_index -= 1

		@li.removeClass 'current'
		jQuery( @li[ @current_index ] ).addClass 'current'

		@button.removeClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is 0 then @first_page.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 ) - 2 then @page_sel_0.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 ) - 1 then @page_sel_1.addClass 'active'
		if jQuery( @li[ @current_index ] ).data('index') is Math.floor( @li.length / 2 )     then @page_sel_2.addClass 'active'


	resize: =>

		@ul.height @li.height()

