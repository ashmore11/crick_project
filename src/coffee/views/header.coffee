module.exports = class Header

	menu_open: false

	constructor: ( @dom ) ->

		document.getElementsByClassName( 'mobile' )[0].addEventListener 'click', => @mobile_clicked()

		jQuery(window).on 'resize', @resize
		@resize()


	mobile_clicked: =>

		if @menu_open is false
			document.getElementsByClassName( 'nav' )[0].style.height  = '160px'
			document.getElementsByClassName( 'nav' )[0].style.display = 'block'
			@menu_open = true
		else
			document.getElementsByClassName( 'nav' )[0].style.height  = '0px'
			document.getElementsByClassName( 'nav' )[0].style.display = 'none'
			@menu_open = false


	resize: =>

		console.log 'header resizing'

		left = window.innerWidth - document.getElementsByClassName('hidden')[0].offsetLeft - document.getElementsByClassName('social')[0].clientWidth - 20

		document.getElementsByClassName('social')[0].style.left = left + 'px'

		if window.innerWidth > 639
			document.getElementsByClassName( 'nav' )[0].style.display = 'block'
			document.getElementsByClassName( 'nav' )[0].style.height  = '38px'
		else
			document.getElementsByClassName( 'nav' )[0].style.height  = '0px'
			document.getElementsByClassName( 'nav' )[0].style.display = 'none'
