delay = ( ms, funk ) -> setTimeout funk, ms

module.exports = class News

	constructor: ( @dom ) ->

		jQuery(window).on 'resize', @resize
		@resize()

		# @page.clone().appendTo @ul for i in [ 0..10 ]


	resize: =>

		if window.innerWidth < 640
			document.getElementsByClassName('section_one')[0].style.height = 'auto'
		else
			document.getElementsByClassName('section_one')[0].style.height = document.getElementsByClassName('bkg')[0].clientHeight + 'px'

		height = document.getElementsByClassName('left_side')[0].innerHeight
		document.getElementsByClassName('right_side')[0].style.height = height + 'px'