settings = 

	platform:

		# The platform [String] 
		id: ''

		# Is a tablet? [Boolean]
		tablet: false
		
		# Is a mobile? [Boolean]
		mobile: false

		# Is desktop? Set after the class definition [Boolean]
		desktop: false

		# Is a tablet or mobile? [Boolean]
		device: false

# Platform settings
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )

	console.log $(window).width(), $(window).height()

	if $(window).width() >= 768 and $(window).height() >= 928 or $(window).width() >= 1024 and $(window).height() >= 692
		settings.platform.mobile = false
		settings.platform.tablet = true
		settings.platform.id     = 'tablet'
	else
		settings.platform.mobile = true
		settings.platform.tablet = false
		settings.platform.id     = 'mobile'

# Set device flag
settings.platform.device = ( settings.platform.tablet or settings.platform.mobile )

# Set desktop flag
if settings.platform.tablet is false and settings.platform.mobile is false
	settings.platform.desktop = true
	settings.platform.id      = 'desktop'

module.exports = settings