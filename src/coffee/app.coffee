settings = require 'app/utils/settings' 
views    = require 'app/controllers/views' 

class App

	constructor: ->

		views.bind()
		

module.exports = new App